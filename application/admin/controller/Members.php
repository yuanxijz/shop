<?php
/** 
    *后台会员控制器类增删改查
    * 
    *
    * @author      3+1项目组.yuanxi
    * @version     2018-02-28 23:34
 */
    namespace app\admin\controller;
    //导入Controller
    use think\Controller;
    //导入Db
    use think\Db;
    class Members extends Allow
    {
    	//加载后台用户列表
        public function getIndex(){
        	//创建请求对象
        	$request=request();
        	//获取搜索的关键词
        	$s=$request->get('keywords');
        	//获取所有数据
            //var_dump($s);exit;
        	$list=Db::table("members")->where('username',"like","%".$s."%")->paginate(2);
        	//加载模板
            return $this->fetch("Members/index",['list'=>$list,'request'=>$request->param(),'s'=>$s]);
            
        }

        //加载添加模板
        public function getAdd(){
            return $this->fetch('user/add');
        }
        

        //执行添加
        public function postInsert(){      
        	//创建请求对象
        	$request=request();
        	//获取插入的数据
        	$data=$request->only(['username','password','email']);
            //md5密码加密
            $data['password']=md5($data['password']);
            $data['status']=1;
        	$data['addtime']=time();
            $data['token'] = rand(1,10000);
            //调用验证方法
            $result=$this->validate($request->param(),'Members');
            //输出错误信息
            if(true !== $result){
                $this->error($result,"/members/add");
            }
        	//执行添加
        	if(Db::table("members")->insert($data)){
        		$this->success("添加成功","/members/index");
        	}else{
        		$this->error("添加失败","/members/add");
        	}


        }

        //执行删除
        public function getDelete($id){
            // echo $id;
            if(Db::table("members")->where('id',$id)->delete()){
                $this->success("删除成功","/members/index");
            }else{
                $this->error("删除失败");
            }
        }

        public function getEdit(){
            $request=request();
            //获取id
            $id=$request->param('id');            
            // var_dump($id);exit;
            //根据id获取需要修改的数据
            $data=Db::table("members")->where("id",$id)->find();

            return $this->fetch('Members/edit',['data'=>$data]);
        }
        //执行修改
        public function postUpdate(){
            $request=request();
            //获取id
            $id=$request->param('id');
            //获取修改以后的值
            $data=$request->only(['username','password',"email"]);
            if(!empty($data['username'])){
                if(!empty($data['password'])){
                    $data['password']=md5($data['password']);
                }else{
                    unset($data['password']);
                }
            }else{
                $this->error("用户名不能为空","/members/edit");
            }
            if(Db::table("members")->where('id',$id)->update($data)){
                $this->success("修改成功","/members/index");
            }else{
                $this->error("修改失败","/members/index");
            }

        }
       
    }
