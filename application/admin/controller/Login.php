<?php
    /** 
    *后台登录
    * 
    *注释要求 
    * @author      3+1项目组.lh
    * @version     2018-02-2 9:40
    */

namespace app\admin\controller;
//导入Controller
use think\Controller;
//导入Db
use think\Db;
//导入session类
use think\Session;
class Login extends Controller
{
	//加载后台登录列表
    public function getLogin()
    {
    	//加载模板
    	return $this->fetch("Login/login");
    }

    //验证码检测
    public function postDologin(){
    	//创建请求对象
    	$request=request();
    	//获取输入的验证码值
    	$fcode=$request->param('fcode');
    	// echo $fcode;
    	//检测输入的验证码是否和系统的验证码值相同
    	if(captcha_check($fcode)){
    		$name=$request->param('name');
    		$pass=$request->param('password');
            $password=MD5($pass);
    		//检测用户名和密码
    		$info=Db::table("admin_user")->where("username='{$name}' and password='{$password}'")->select();
    		if($info){
    			//设置用户登录信息写入到session
    			Session::set('islogin',$name);
                //1.获取当前后台管理员用户所具有的权限信息
                $list=Db::query("select distinct n.name,n.mname,n.aname from admin_user as au,role_node as rn,node as n,role as r where r.id={$info[0]['rid']} and rn.nid = n.id and rn.rid = r.id");
                // var_dump($list);exit;
                //2.初始化权限列表 给管理员添加访问后台首页的权限
        
                $nodelist['admin'][]="getindex";
                //遍历
                foreach($list as $key=>$value){
                    $nodelist[$value['mname']][]=$value['aname'];
                    //如果具有add  添加insert
                    if($value['aname']=='getadd'){
                        $nodelist[$value['mname']][]="postinsert";
                    }

                    //如果有edit 添加update
                    if($value['aname']=="getedit"){
                        $nodelist[$value['mname']][]="postupdate";
                    }
                }
                // echo "<pre>";
                // var_dump($nodelist);exit;
                //3.存储在session里
                Session::set("nodelist",$nodelist);
                //4.对比(访问模块和权限列表)
    			$this->success("登录成功","/admin/index");
    		}else{
    			$this->error("用户名或者密码有误","/adminlogin/login");
    		}
    		
    	}else{
    		// echo "error";
    		$this->error("验证码输入错误","/adminlogin/login");
    	}

    }

    //退出
    public function getLogout(){
    	//销毁session
    	Session::delete('islogin');
    	$this->success("退出成功","/adminlogin/login");
    }
}
