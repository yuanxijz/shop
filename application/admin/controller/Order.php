<?php
/** 
	*后台订单控制器类
	* 
	* 
	* @author      3+1项目组.freedom
	* @version     2018-01-30 19:45
*/	
	namespace app\admin\controller;
	use think\Controller;
	use think\Db;
	Class Order extends Allow
	{
		//后台订单列表
		public function getIndex(){
			//创建请求
			$request = request();
			//获取搜索的关键词
			$s = $request->get('keywords');
			//获取所有数据+搜索+分页
			$list = Db::table('order')->where('orderid','like','%'.$s.'%')->paginate(5);
			//加载后台订单列表模板
			return $this->fetch('Orderlist/index',['list'=>$list,'request'=>$request->param(),'s'=>$s]);
		}
		/**
		 * 发货
		 * @author yuanxi
		 * @DateTime 2018-03-06T20:32:03+0800
		 * @return   [type]                   [description]
		 */
		public function getDeliver(){
			$request = request();
			$id = $request->param('id');
			if (Db::table('order')->where('id',$id)->setField('status',2)) {
				//获取订单详情表oid=订单表的id的数据
				$infos = Db::table('orderdetail')->where('oid',$id)->select();
				foreach ($infos as $key => $value) {
					//订单表已经发货，修改订单详情表当前订单下的所有数据状态为2
					Db::table('orderdetail')->where('id',$value['id'])->setField('status',2);
				}
				// exit;
				$this->success("发货成功","/adminorder/index","",1);
			}else{
				$this->error("发货失败","/adminorder/index","",1);
			}
		}
	}