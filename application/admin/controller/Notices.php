<?php
/** 
	*后台公告管理控制器类
	* 
	*
	* @author      3+1项目组.yuanxi
	* @version     2018-01-31 20:51
*/
	namespace app\admin\controller;
	use think\Controller;
	use think\Db;
	class Notices extends Allow{
			
		//后台公告列表
		public function getIndex(){
			$request=request();
			$keywords=$request->get('keywords');
	        $list=Db::table("notices")->where("title",'like',"%".$keywords."%")->paginate(5);
			return $this->fetch("Notices/index",['list'=>$list,'request'=>$request->param(),'keywords'=>$keywords]);
		}


		//加载添加模板
		public function getAdd(){
			return $this->fetch("Notices/add");
		}
		//添加
		public function postInsert(){
			// echo 111;exit;
			//创建应用请求
			$request=request();
			//验证名称、content字段
			$result = $this->validate($request->only(['title','content']),'Notices');
			if($result !== true){
    			$this->error($result,"/notices/add");
    		}
    		//将即将执行添加操作的数据放入到一个数组
	    	$data = $request->only(['title','content','desc','status']);
			//如果上传图片，就执行图片上传
    		$file = $request->file('pic');
    		if($file == true){
    			$res = $this->validate(['fia'=>$file],['fia'=>'require|image|fileSize:10240000'],['fia.require'=>'上传文件为空','fia.image'=>'上传文件类型必须是图像类型','fia.fileSize'=>'图片大小不能超过1M']);//bug:图片上传大小限制没做
		    	if($res !== true){
		    		$this->error($res,"/notices/add");
		    	}
		    	$info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
		    	if(!$info){
		    		$this->error($info,"/notices/index");
		    	}
		    	$savename = $info->getSaveName();
	    		//拼接图片路径
	    		// echo $savename;
	    		//20180201\874389dbab446daf794c2e4209d9d557.jpg
	    		//将$savename中\替换成/
	    		$savename = str_replace("\\", "/", $savename);
	    		//获取图片资源路径
	    		$picpath = '/public/uploads'.'/'.$savename;
	    		// echo $picpath;exit;
	    		//将图片资源路径赋值给数组中pic字段
	    		$data['pic']=$picpath;
	    	}
	    	//获取本地时间戳
	    	$data['addtime']=time();
    		// var_dump($data);exit;
    		//执行添加操作
    		if(Db::table("notices")->insert($data)){
    			$this->success("公告添加成功","/notices/index");
    		}else{
    			$this->error("公告添加失败","/notices/add");
    		}
		}
		//公告删除
		public function getDelete($id){
			//获取要删除图片数据
			$data = Db::table("notices")->where('id',$id)->find();
			// echo "<pre>";
			// var_dump($data['pic']);exit;
			//判断是否有图片
			if($data['pic'] !== null){
				//拼接要删除的图片资源路径
				$path = ROOT_PATH.'public'.DS.$data['pic'];
			}			
			//执行删除操作
			if(Db::table("notices")->where('id',$id)->delete()){
				if($data['pic'] !== null){
					//删除成功 同时删除图片
					unlink($path);
				}
				$this->success("公告删除成功","/notices/index");
			}else{
				$this->error("公告删除失败","/notices/index");
			}
		}

		//edit操作
		public function getEdit($id){
			$data = Db::table("notices")->where('id',$id)->find();
			return $this->fetch("Notices/edit",['data'=>$data]);
		}

		//更新操作
		public function postUpdate($id){
			//创建应用请求
			$request=request();
			//获取要删除的数据
			$data = Db::table("notices")->where('id',$id)->find();
			//将获取的数据放入到一个数组
	    	$data = $request->only(['title','desc','addtime','content','status']);  
			//判断原本是否有图片
			if($_POST['pic']){
				//如果有
				//如果修改图片
	    		$file = $request->file('pics');
	    		if($file){
					//拼接要删除的旧图片资源路径
					$path = ROOT_PATH.'public'.DS.$data['pic'];
	    			$res = $this->validate(['file1'=>$file],['file1'=>'image'],['file1.image'=>'上传文件类型必须是图像类型']);//bug:图片上传大小限制没做
			    	if($res !== true){
			    		$this->error($res,"/notices/index");
			    	}
			    	//上传成功 同时删除旧图片
					unlink($path);
			    	$info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
			    	if(!$info){
			    		$this->error($info,"/notices/index");
			    	}
			    	$savename = $info->getSaveName();
		    		//拼接图片路径
		    		// echo $savename;
		    		//20180201\874389dbab446daf794c2e4209d9d557.jpg
		    		//将$savename中\替换成/
		    		$savename = str_replace("\\", "/", $savename);
		    		//获取图片资源路径
		    		$picpath = '/public/uploads'.'/'.$savename;	
	    		}else{
	    			//如果没有修改图片，获取图片资源路径
	    			$picpath = $_POST['pic'];
	    		}
	    		//将图片资源路径赋值给数组中pic字段
	    		$data['pic']=$picpath;
	    		// var_dump($data);exit;
			} 
			 		
    		//执行更新操作
    		if(Db::table("notices")->where('id',$id)->update($data)){
    			$this->success("公告修改成功","/notices/index");
    		}else{
    			$this->error("公告修改失败","/notices/index");
    		}


		}
	}

?>