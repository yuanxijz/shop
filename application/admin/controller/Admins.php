<?php
/** 
    *后台权限控制 角色分配
    * 
    *注释要求 
    * @author      3+1项目组.lh
    * @version     2018-02-8 20:08
*/
namespace app\admin\controller;
//导入Controller
use think\Controller;
//导入模型类
use app\admin\model\Admins;
use think\Db;
class Admins extends Allow
{
	//加载后台首页
    public function getIndex()
    {

    	//使用query类查询数据
    	$info=Admins::paginate(2);
    	//加载模板 分配数据;
        
    	return $this->fetch("Admins/index",['info'=>$info]);

    }

    //删除
    public function getDelete($id){
    	// echo $id;
    	if(Admins::where('id',$id)->delete()){
    		$this->success("删除成功","/adminuser/index");
    	}
    }

    //分配角色
    public function getRolelist(){
        //创建请求对象
        $request=request();
        $id=$request->param('id');
        //var_dump($id);exit;
        //获取用户信息
        $info=Db::table("admin_user")->where('id',"{$id}")->find();  
        //获取所有的角色信息
        $role=Db::table("role")->select();
        //获取当前用户所具有的角色信息
        $data=Db::table('admin_user')->where('id',"{$id}")->select();
        //遍历
          $rids[]=$data[0]['rid'];
        
        //加载模板 分配数据
        return $this->fetch("Admins/rolelist",['info'=>$info,'role'=>$role,'rids'=>$rids]);
    }

   // 保存角色
    public function postSaverole(){
        //创建请求对象
        $request=request();
        //获取用户id
        $id=$request->param('id');
        $rid=$_POST['rid'];
        //修改管理员角色
        Db::table('admin_user')->where('id', $id)->update(['rid' => $rid]);
        $this->success("角色分配成功","/adminuser/index");
    }

    //添加
    public function getAdd(){
        return $this->fetch('admins/add');
    }

    //执行添加
    public function postInsert(){
          
            //创建请求对象
            $request=request();
            //获取插入的数据

            $data=$request->only(['username','password','email']);
            //md5密码加密
            $data['password']=md5($data['password']);
            $data['status']=1;
            $data['addtime']=time();

            //调用验证方法
            $result=$this->validate($request->param(),'User');
            //输出错误信息
            if(true !== $result){
                $this->error($result,"/user/add");
            }
            //执行添加
            if(Db::table("admin_user")->insert($data)){
                $this->success("添加成功","/user/index");
            }else{
                $this->error("添加失败","/user/add");
            }


    }

    //修改
    public function getEdit($id){
        //var_dump($id);
        $data=Db::table('admin_user')->where('id',$id)->find();
        //var_dump($data);
        return $this->fetch('admins/edit',['data'=>$data]);
    }

    //执行修改
    public function postUpdate(){
        $request=request();
        //获取id
        $id=$request->param('id');
        //获取修改以后的值

        $data=$request->only(['username','password',"email"]);
        if(!empty($data['password'])){
            $data['password']=md5($data['password']);
        }else{
            unset($data['password']);
        }
        // var_dump($id);exit;

        if(Db::table("admin_user")->where('id',$id)->update($data)){
            $this->success("修改成功","/user/index");
        }else{
            $this->error("修改失败");
        };
    }
}
