<?php
/** 
    *后台商品模块控制器类
    * 
    *
    * @author      3+1项目组.hzy
    * @version     2018-02-03 21:22
 */
namespace app\admin\controller;
//导入Controller
use think\Controller;
//导入Db
use think\Db;
class Goods extends Allow
{
    //列表
    public function getIndex()
    {
        $goods=Db::table("goods")->select();
       return $this->fetch('Goods/index',['goods'=>$goods]);
    }
    //加载添加模块
    public function getAdd()
    {
        //获取所有分类信息
        $cate=Db::table("brands")->select();
       return $this->fetch('Goods/add',['cate'=>$cate]);
    }
    //执行添加
    public function postInsert()
    {
        //创建请求对象
        $request=request();
        //调用验证方法
        $result=$this->validate($request->param(),'Goods');
        //输出错误信息
        if(true !== $result){
            $this->error($result,'/goods/add');
        }
        //先执行图片上传
        $file = $request->file('pic');
        $res = $this->validate(['file1'=>$file],['file1'=>'require|image|fileSize:1024000'],['file1.require'=>'上传文件为空','file1.image'=>'上传文件类型必须是图像类型','file1.fileSize'=>'图片大小不能超过1M']);//bug:图片上传大小限制没做
        if($res !== true){
            $this->error($res,"/goods/index");
        }
        $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
        if(!$info){
            $this->error($info,"/goods/index");
        }
        $savename = $info->getSaveName();
        //拼接图片路径
        // echo $savename;
        //20180201\874389dbab446daf794c2e4209d9d557.jpg
        //将$savename中\替换成/
        $savename = str_replace("\\", "/", $savename);
        //获取图片资源路径
        $picpath = '/public/uploads'.'/'.$savename;
        //echo $picpath;exit;
        //将获取的数据放入到一个数组
        $data = $request->only(['name','color','status','size','brands_id','store','price','content']);
        //将图片资源路径赋值给数组中pic字段
        $data['pic']=$picpath;
        $data['addtime']=time();
        //var_dump($data);exit;
        //插入数据库
        //执行添加操作
        if(Db::table("goods")->insert($data)){
            $this->success("商品添加成功","/goods/index");
        }else{
            $this->error("商品添加失败","/goods/add");
        }
    }
    //执行删除
    public function getDelete($id)
    {
        //获取要删除图片数据
        $data = Db::table("goods")->where('id',$id)->find();
        //拼接要删除的图片资源路径
        $path = ROOT_PATH.'public'.DS.$data['pic'];
        preg_match_all('/<img.*?src="(.*?)".*?>/is',$data['content'],$arr);
        // echo "<pre>";
        // var_dump($arr);exit;
        //执行删除操作
        if(Db::table("goods")->where('id',$id)->delete()){
            //删除成功 同时删除图片
            unlink($path);
            //删除百度编辑器图片
            foreach($arr[1] as $key=>$value){
                unlink(".".$value);
            }
            $this->success("商品删除成功","/goods/index");
        }else{
            $this->error("商品删除失败","/goods/index");
        }
    }
    //执行修改
    public function getEdit()
    {
        $request=request();
        //获取id
        $id=$request->param('id');
        $data=Db::table('brands')->select();
        $info=Db::table('goods')->where('id',$id)->find();
        return $this->fetch('Goods/edit',['info'=>$info,'data'=>$data]);
    }
    //执行修改
    public function postUpdate($id){
        $request=request();
        //如果修改图片
        $file = $request->file('pics');
        if($file){
            //获取要删除的旧图片数据
            $data = Db::table("goods")->where('id',$id)->find();
            //拼接要删除的旧图片资源路径
            $path = ROOT_PATH.'public'.DS.$data['pic'];
            $res = $this->validate(['file1'=>$file],['file1'=>'image'],['file1.image'=>'上传文件类型必须是图像类型']);//bug:图片上传大小限制没做
            if($res !== true){
               $this->error($res,"/goods/index");
            }
            //上传成功 同时删除旧图片
            unlink($path);
            $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
            if(!$info){
                $this->error($info,"/goods/index");
            }
            $savename = $info->getSaveName();
            //拼接图片路径
            // echo $savename;
            //将$savename中\替换成/
            $savename = str_replace("\\", "/", $savename);
            //获取图片资源路径
            $picpath = '/public/uploads'.'/'.$savename;    
        }else{
            //如果没有修改图片，获取图片资源路径
            $picpath = $_POST['pic'];
        }
        //将获取的数据放入到一个数组
        $data = $request->only(['name','color','status','size','brands_id','store','price','content']);
        //将图片资源路径赋值给数组中pic字段
        $data['pic']=$picpath;
        $data['addtime']=time();
        //var_dump($data);exit;
        if(Db::table("goods")->where('id',$id)->update($data)){
            $this->success("商品修改成功","/goods/index");
        }else{
            $this->error("商品修改失败","/goods/index");
        }
    }
}
