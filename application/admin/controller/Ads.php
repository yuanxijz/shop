<?php
/** 
	*后台广告管理控制器类
	* 
	*
	* @author      3+1项目组.yuanxi
	* @version     2018-01-31 20:31
*/
	namespace app\admin\controller;
	use think\Controller;
	use think\Db;
	class Ads extends Allow{
			
		//后台广告列表
		public function getIndex(){
			$request=request();
			$keywords=$request->get('keywords');
	        $list=Db::table("ads")->where("name",'like',"%".$keywords."%")->paginate(5);
			return $this->fetch("Ads/index",['list'=>$list,'request'=>$request->param(),'keywords'=>$keywords]);
		}


		//加载添加模板
		public function getAdd(){
			return $this->fetch("Ads/add");
		}
		//添加
		public function postInsert(){
			// echo 111;exit;
			//创建应用请求
			$request=request();
			//验证名称、adurl字段
			$result = $this->validate($request->only(['name','adurl']),'Ads');
			if($result !== true){
    			$this->error($result,"/ads/add");
    		}
			//先执行图片上传
    		$file = $request->file('pic');
    		$res = $this->validate(['fia'=>$file],['fia'=>'require|image|fileSize:102400000000'],['fia.require'=>'上传文件为空','fia.image'=>'上传文件类型必须是图像类型','fia.fileSize'=>'图片大小不能超过1M']);//bug:图片上传大小限制没做
	    	if($res !== true){
	    		$this->error($res,"/ads/add");
	    	}
	    	$info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
	    	if(!$info){
	    		$this->error($info,"/ads/index");
	    	}
	    	$savename = $info->getSaveName();
    		//拼接图片路径
    		// echo $savename;
    		//20180201\874389dbab446daf794c2e4209d9d557.jpg
    		//将$savename中\替换成/
    		$savename = str_replace("\\", "/", $savename);
    		//获取图片资源路径
    		$picpath = '/public/uploads'.'/'.$savename;
    		// echo $picpath;exit;
    		//将获取的数据放入到一个数组
    		$data = $request->only(['name','adurl','status']);
    		//将图片资源路径赋值给数组中pic字段
    		$data['pic']=$picpath;
    		// var_dump($data);exit;
    		//执行添加操作
    		if(Db::table("ads")->insert($data)){
    			$this->success("广告添加成功","/ads/index");
    		}else{
    			$this->error("广告添加失败","/ads/add");
    		}
		}
		//广告删除
		public function getDelete($id){
			//获取要删除图片数据
			$data = Db::table("ads")->where('id',$id)->find();
			//拼接要删除的图片资源路径
			$path = ROOT_PATH.'public'.DS.$data['pic'];
			//执行删除操作
			if(Db::table("ads")->where('id',$id)->delete()){
				//删除成功 同时删除图片
				unlink($path);
				$this->success("广告删除成功","/ads/index");
			}else{
				$this->error("广告删除失败","/ads/index");
			}
		}

		//edit操作
		public function getEdit($id){
			$data = Db::table("ads")->where('id',$id)->find();
			return $this->fetch("Ads/edit",['data'=>$data]);
		}

		//更新操作
		public function postUpdate($id){
			//创建应用请求
			$request=request();

    		//如果修改图片
    		$file = $request->file('pics');
    		if($file){
    			//获取要删除的旧图片数据
				$data = Db::table("ads")->where('id',$id)->find();
				//拼接要删除的旧图片资源路径
				$path = ROOT_PATH.'public'.DS.$data['pic'];
    			$res = $this->validate(['file1'=>$file],['file1'=>'image'],['file1.image'=>'上传文件类型必须是图像类型']);//bug:图片上传大小限制没做
		    	if($res !== true){
		    		$this->error($res,"/ads/index");
		    	}
		    	//上传成功 同时删除旧图片
				unlink($path);
		    	$info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
		    	if(!$info){
		    		$this->error($info,"/ads/index");
		    	}
		    	$savename = $info->getSaveName();
	    		//拼接图片路径
	    		// echo $savename;
	    		//20180201\874389dbab446daf794c2e4209d9d557.jpg
	    		//将$savename中\替换成/
	    		$savename = str_replace("\\", "/", $savename);
	    		//获取图片资源路径
	    		$picpath = '/public/uploads'.'/'.$savename;	
    		}else{
    			//如果没有修改图片，获取图片资源路径
    			$picpath = $_POST['pic'];
    		}
    		//将获取的数据放入到一个数组
    		$data = $request->only(['name','adurl','status']);
    		//将图片资源路径赋值给数组中pic字段
    		$data['pic']=$picpath;
    		// var_dump($data);exit;
    		//执行添加操作
    		if(Db::table("ads")->where('id',$id)->update($data)){
    			$this->success("广告修改成功","/ads/index");
    		}else{
    			$this->error("广告修改失败","/ads/add");
    		}


		}
	}

?>