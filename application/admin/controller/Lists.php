<?php
/** 
    *后台节点增删改查界面
    * 
    *注释要求 
    * @author      3+1项目组.lh
    * @version     2018-02-8 20:08
*/
namespace app\admin\controller;
//导入Controller
use think\Controller;
use think\Db;
class Lists extends Allow
{
    public function getIndex()
    {
    	//获取节点信息
    	$node=Db::table("node")->order('mname desc')->select();
    	//加载模板
    	return $this->fetch("Lists/index",['node'=>$node]);
    }

 //添加
    public function getAdd()
    {
    	
    	//加载模板
    	return $this->fetch("Lists/add");
    }

    //执行添加
    public function postInsert(){
    	//创建请求对象
    	$request=request();
    	//获取插入的数据
    	$data=$request->only(['name','mname','aname']);
    	$data['status']=1;
        //调用验证方法
        $result=$this->validate($request->param(),'Lists');
        //输出错误信息
        if(true !== $result){
            $this->error($result,"/lists/add");
        }
    	if(Db::table('node')->insert($data)){
    		$this->success('添加成功','/lists/index');
    	}else{
    		$this->error('添加失败','/lists/index');
    	} 
    }

    //删除
    public function getDel(){
    	//创建请求对象
    	$request=request();
    	$id=$request->param('id');
    	//var_dump($id);exit;
 		if(Db::table('node')->where('id',$id)->delete()){
    		$this->success('删除成功','/lists/index');
    	}else{
    		$this->error('删除失败','/lists/index');
    	}   	
    }

    //修改
    public function getEdit($id){
    	//根据id获取需要修改的数据

          $data=Db::table("node")->where("id",$id)->find();
          //var_dump($data);
          return $this->fetch('Lists/edit',['data'=>$data]);
    }

    //执行修改
    public function postUpdate(){
    	$request=request();
        //获取id
        $id=$request->param('id');
        //获取修改以后的值
    	
    	 $data=$request->only(['name','mname',"aname"]);
    	//var_dump($data);exit; 
    	 if(Db::table("node")->where('id',$id)->update($data)){
              $this->success("修改成功","/lists/index");
          }else{
              $this->error("修改失败","/lists/index");
          }
    }
}
