<?php
/** 
    *后台用户控制器类增删改查
    * 
    *
    * @author      3+1项目组.lh
    * @version     2018-01-30 19:45
 */
    namespace app\admin\controller;
    //导入Controller
    use think\Controller;
    //导入Db
    use think\Db;
    class User extends Allow
    {
    	//加载后台用户列表
        public function getIndex(){
        	//创建请求对象
        	$request=request();
        	//获取搜索的关键词
        	$s=$request->get('keywords');
        	//获取所有数据
            //var_dump($s);exit;
        	$list=Db::table("admin_user")->where('username',"like","%".$s."%")->paginate(2);
        	//加载模板
             $a=date('Y-m-d H:i:s',$list[0]['addtime']);        
             //$list[0]['addtime']=$a;
             // print_r($list[0]['addtime']);exit;
             // echo "<pre>";
             // print_r($list);
            return $this->fetch("User/index",['list'=>$list,'a'=>$a,'request'=>$request->param(),'s'=>$s]);
            
        }

        //加载添加模板
        public function getAdd(){
            return $this->fetch('user/add');
        }
        

        //执行添加
        public function postInsert(){      
        	//创建请求对象
        	$request=request();
        	//获取插入的数据
        	$data=$request->only(['username','password','email']);
            //md5密码加密
            $data['password']=md5($data['password']);
            $data['status']=1;
        	$data['addtime']=time();
             // echo "<pre>";
              // var_dump($data);exit;
             //$data['time']=time();

            //调用验证方法
            $result=$this->validate($request->param(),'User');
            //输出错误信息
            if(true !== $result){
                $this->error($result,"/user/add");
            }
        	//执行添加
        	if(Db::table("admin_user")->insert($data)){
        		$this->success("添加成功","/user/index");
        	}else{
        		$this->error("添加失败","/user/add");
        	}


        }

        //执行删除
        public function getDelete($id){
            // echo $id;
            if(Db::table("admin_user")->where('id',$id)->delete()){
                $this->success("删除成功","/user/index");
            }else{
                $this->error("删除失败");
            }
        }

        public function getEdit(){
            $request=request();
            //获取id
            $id=$request->param('id');            
            // var_dump($id);exit;
            //根据id获取需要修改的数据
            $data=Db::table("admin_user")->where("id",$id)->find();

            return $this->fetch('User/edit',['data'=>$data]);
        }

        //执行修改
        public function postUpdate(){
            $request=request();
            //获取id
            $id=$request->param('id');
            //获取修改以后的值
            $data=$request->only(['username','password',"email"]);
            if(!empty($data['username'])){
                if(!empty($data['password'])){
                    $data['password']=md5($data['password']);
                }else{
                    unset($data['password']);
                }
            }else{
                $this->error("用户名不能为空","/user/edit");
            }
           
            // var_dump($id);exit;
             

            if(Db::table("admin_user")->where('id',$id)->update($data)){
                $this->success("修改成功","/user/index");
            }else{
                $this->error("修改失败","/user/index");
            }

        }
    }
