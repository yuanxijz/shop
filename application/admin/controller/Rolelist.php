<?php
/** 
    *后台角色界面 增删改查 权限分配
    * 
    *注释要求 
    * @author      3+1项目组.lh
    * @version     2018-02-8 20:08
*/
namespace app\admin\controller;
//导入Controller
use think\Controller;
use think\Db;
class Rolelist extends Allow
{
    public function getIndex()
    {
    	//获取角色信息
    	$role=Db::table("role")->select();
        // var_dump($role);
    	//加载模板
    	return $this->fetch("Rolelist/index",['role'=>$role]);
    }


    //分配权限
    public function getSaverole(){
        //创建请求对象
        $request=request();
        $id=$request->param('id');
        //获取角色信息
        $info=Db::table('role')->where('id',"{$id}")->find();
        //var_dump($info);

        //获取所有权限信息
        $role=Db::table('node')->select();
        //var_dump($role);
        //获取当前用户所具有的权限信息
        $data=Db::table('role_node')->where('rid',"{$id}")->select();
        //var_dump($data);
        //遍历权限信息rid
        foreach ($data as $v) {
            $nids[]=$v['nid'];
        }
        if(empty($nids)){
            $nids[]=1;
       }
        //var_dump($nids);
       return $this->fetch("rolelist/saverole",['info'=>$info,'role'=>$role,'nids'=>$nids]);
        
        //加载模板，分配数据


    }

    //保存分配
    public function postDosaverole(){
    	$request=request();
    	$rid=$request->param('rid');
    	// var_dump($rid);
    	//删除当前用户
    	 Db::table('role_node')->where('rid',$rid)->delete();
    	//获取新权限ID
        $nid=$_POST['nid'];
    	// var_dump($nid); 
    	
    	//遍历插入
    	 foreach ($nid as $value) {
    	 	$data['rid']=$rid;
    	 	$data['nid']=$value;
    		   // var_dump($data);
    		//执行插入
    		Db::table('role_node')->insert($data);
        }   
                 $this->success("保存成功","/rolelist/index");	 
    }

    //角色添加
     //加载添加模板
    public function getAdd(){
        return $this->fetch('rolelist/add');
    }

    //执行添加  
    public function postInsert(){
        //创建请求对象
            $request=request();
        //获取插入的数据
        $data=$request->only(['name']);
        $data['status']=1;
        //执行添加
        if(Db::table("role")->insert($data)){
            $this->success("添加成功","/rolelist/index");
        }else{
            $this->error("添加失败","/rolelist/add");
        }
    } 

    //删除角色
    public function getDelete($id){
            // echo $id;
            if(Db::table("role")->where('id',$id)->delete()){
                $this->success("删除成功","/rolelist/index");
            }else{
                $this->error("删除失败","/rolelist/index");
            }
    }
    //修改角色
    public function getEdit($id){
        //var_dump($id);
        $data=Db::table('role')->where('id',$id)->find();
        //var_dump($data);
        return $this->fetch('rolelist/edit',['data'=>$data]);
    }

    //执行修改
    public function postUpdate(){
        $request=request();
        //获取id
        $id=$request->param('id');
        //获取修改以后的值
        $data=$request->only(['name']);
        if(Db::table("role")->where('id',$id)->update($data)){
            $this->success("修改成功","/rolelist/index");
        }else{
            $this->error("修改失败","/rolelist/index");
        }
    }
}
