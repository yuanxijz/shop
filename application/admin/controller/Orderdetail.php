<?php
/** 
	*后台订单控制器类
	* 
	* 
	* @author      3+1项目组.freedom
	* @version     2018-01-30 19:45
*/	
	namespace app\admin\controller;
	use think\Controller;
	use think\Db;
	Class Orderdetail extends Controller
	{
		/**
		 * 订单详情
		 * @author yuanxi
		 * @DateTime 2018-03-06T09:30:24+0800
		 */
		public function getOrderdetail(){
			$request = request();
			$oid = $request->param('id');
			$info = Db::table('order')->alias('o')->join('orderdetail od','od.oid=o.id')->field('od.status as ods,o.id as id,od.id as oid,od.goodsname as goodsname,od.gnum as gnum,od.price as price,od.pic as pic')->where('od.oid',$oid)->paginate(2);
			$data = Db::table('order')->where('id',$oid)->find();
			return $this->fetch('Orderlist/orderdetail',['info'=>$info,'data'=>$data]);
		}
		
	}