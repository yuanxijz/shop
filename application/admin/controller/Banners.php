<?php
/** 
	*后台banner管理控制器类
	* 
	*
	* @author      3+1项目组.yuanxi
	* @version     2018-02-02 19:46
*/
	namespace app\admin\controller;
	use think\Controller;
	use think\Db;
	class Banners extends Allow{
			
		//后台广告列表
		public function getIndex(){
			$request=request();
			$keywords=$request->get('keywords');
	        $list=Db::table("banners")->where("name",'like',"%".$keywords."%")->paginate(5);
			return $this->fetch("Banners/index",['list'=>$list,'request'=>$request->param(),'keywords'=>$keywords]);
		}


		//加载添加模板
		public function getAdd(){
			return $this->fetch("Banners/add");
		}
		//添加
		public function postInsert(){
			// echo 111;exit;
			//创建应用请求
			$request=request();
			//验证名称、url字段
			$result = $this->validate($request->only(['name','url']),'Banners');
			if($result !== true){
    			$this->error($result,"/banners/add");
    		}
			//先执行图片上传
    		$file = $request->file('pic');
    		$res = $this->validate(['fia'=>$file],['fia'=>'require|image'],['fia.require'=>'上传文件为空','fia.image'=>'上传文件类型必须是图像类型']);//bug:图片上传大小限制没做
	    	if($res !== true){
	    		$this->error($res,"/banners/index");
	    	}
	    	$info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
	    	if(!$info){
	    		$this->error($info,"/banners/index");
	    	}
	    	$savename = $info->getSaveName();
    		//拼接图片路径
    		// echo $savename;
    		//20180201\874389dbab446daf794c2e4209d9d557.jpg
    		//将$savename中\替换成/
    		$savename = str_replace("\\", "/", $savename);
    		//获取图片资源路径
    		$picpath = '/public/uploads'.'/'.$savename;
    		// echo $picpath;exit;
    		//将获取的数据放入到一个数组
    		$data = $request->only(['name','url','status']);
    		//将图片资源路径赋值给数组中pic字段
    		$data['pic']=$picpath;
    		// var_dump($data);exit;
    		//执行添加操作
    		if(Db::table("banners")->insert($data)){
    			$this->success("Banner添加成功","/banners/index");
    		}else{
    			$this->error("Banner添加失败","/banners/add");
    		}
		}
		//广告删除
		public function getDelete($id){
			//获取要删除图片数据
			$data = Db::table("banners")->where('id',$id)->find();
			//拼接要删除的图片资源路径
			$path = ROOT_PATH.'public'.DS.$data['pic'];
			//执行删除操作
			if(Db::table("banners")->where('id',$id)->delete()){
				//删除成功 同时删除图片
				unlink($path);
				$this->success("Banner删除成功","/banners/index");
			}else{
				$this->error("Banner删除失败","/banners/index");
			}
		}

		//edit操作
		public function getEdit($id){
			$data = Db::table("banners")->where('id',$id)->find();
			return $this->fetch("Banners/edit",['data'=>$data]);
		}

		//更新操作
		public function postUpdate($id){
			//创建应用请求
			$request=request();

    		//如果修改图片
    		$file = $request->file('pics');
    		if($file){
    			//获取要删除的旧图片数据
				$data = Db::table("banners")->where('id',$id)->find();
				//拼接要删除的旧图片资源路径
				$path = ROOT_PATH.'public'.DS.$data['pic'];
    			$res = $this->validate(['file1'=>$file],['file1'=>'image'],['file1.image'=>'上传文件类型必须是图像类型']);//bug:图片上传大小限制没做
		    	if($res !== true){
		    		$this->error($res,"/banners/index");
		    	}
		    	//上传成功 同时删除旧图片
				unlink($path);
		    	$info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
		    	if(!$info){
		    		$this->error($info,"/banners/index");
		    	}
		    	$savename = $info->getSaveName();
	    		//拼接图片路径
	    		// echo $savename;
	    		//20180201\874389dbab446daf794c2e4209d9d557.jpg
	    		//将$savename中\替换成/
	    		$savename = str_replace("\\", "/", $savename);
	    		//获取图片资源路径
	    		$picpath = '/public/uploads'.'/'.$savename;	
    		}else{
    			//如果没有修改图片，获取图片资源路径
    			$picpath = $_POST['pic'];
    		}
    		//将获取的数据放入到一个数组
    		$data = $request->only(['name','url','status']);
    		//将图片资源路径赋值给数组中pic字段
    		$data['pic']=$picpath;
    		// var_dump($data);exit;
    		//执行添加操作
    		if(Db::table("banners")->where('id',$id)->update($data)){
    			$this->success("Banner修改成功","/banners/index");
    		}else{
    			$this->error("Banner修改失败","/banners/add");
    		}


		}
	}

?>