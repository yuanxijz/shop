<?php
/** 
    *后台首页控制器类
    * 
    *注释要求 
    * @author      3+1项目组.lh
    * @version     2018-02-2 9:40
*/
    namespace app\admin\controller;
    //导入Controller
    use think\Controller;

    class Admin extends Allow
    {
    	//加载后台首页
        public function getIndex()
        {
        	//加载模板
        	return $this->fetch("Admin/index");
        }
    }
