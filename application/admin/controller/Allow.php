<?php
/** 
    *后台登录初始化文件
    * 
    *注释要求 
    * @author      3+1项目组.lh
    * @version     2018-02-2 10:00
*/
	namespace app\admin\controller;
	//导入Controller
	use think\Controller;
	//导入session
	use think\Session;
	class Allow extends Controller
	{
		//初始化方法
		public function _initialize(){
			//检测是否具有用户的登录session信息
			if(!Session::get('islogin')){
				//跳转到登录界面
				$this->error("当前用户未登录，3秒后跳转登录界面","/adminlogin/login");
			}

			//获取管理员访问模块 控制器跟方法 
			//创建请求
			$request=request();
			//获取当前管理员访问模块 控制器和方法
			$controller=strtolower($request->controller());
			$action=$request->action();
			 // echo $controller.":".$action;
			//获取session信息
			$nodelist=Session::get('nodelist');
			// echo "<pre>";
           // var_dump($nodelist);exit;
			//对比访问的控制器和方法是否在session数组里
			if(empty($nodelist[$controller]) || !in_array($action,$nodelist[$controller])){
				$this->error("抱歉,您没有权限访问当前模块,请联系超级管理员");
			}
		}
	}
