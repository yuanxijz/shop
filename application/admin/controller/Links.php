<?php
/** 
	*后台友情链接控制器类
	* 
	*
	* @author      3+1项目组.yuanxi
	* @version     2018-02-02 14:45
*/

	namespace app\admin\controller;
	use think\Controller;
	use think\Db;

	class Links extends Allow{
		//友情链接列表
		public function getIndex(){
			//创建请求
			$request = request();
			//获取搜索数据
			$keywords = $request->get('keywords');
			//从数据库读取数据
			$list = Db::table("links")->where("name","like","%".$keywords."%")->paginate(5);
			//加载列表模板和传递数据
			return $this->fetch("Links/index",['list'=>$list,'request'=>$request->param(),'keywords'=>$keywords]);
		}
		//加载友情链接添加模板
		public function getAdd(){
			return $this->fetch("Links/add");
		}
		//添加操作
		public function postInsert(){
			//创建请求应用
			$request=request();
			//调用验证器验证
			$result = $this->validate($request->only(['name','linkurl','linkemail']),'Links');
			if($result !== true){
				$this->error($result,"/links/add");
			}
			//获取数据
			$data = $request->only(['name','linkurl','linkemail','status']);
			//执行添加操作
			if(Db::table("links")->insert($data)){
				$this->success("添加成功","/links/index");
			}else{
				$this->error("添加失败","/links/add");
			}

		}


		//删除
		public function getDelete($id){
			if(Db::table('links')->where('id',$id)->delete()){
				$this->success("友情链接删除成功","/links/index");
			}else{
				$this->error("友情链接删除失败","/links/index");
			}
		}


		//edit操作
		public function getEdit($id){
			//获取数据
			$data = Db::table("links")->where('id',$id)->find();
			return $this->fetch("Links/edit",["data"=>$data]);
		}

		//更新操作
		public function postUpdate(){
			//创建请求
			$request = request();
			//获取ID
			$id = $request->param('id');
			// echo $id;exit;
			//获取数据
			$data = $request->only(['name','linkurl','linkemail','status']);
			//执行更新操作
			if(Db::table("links")->where("id",$id)->update($data)){
				$this->success("修改成功","/links/index");
			}else{
				$this->error("修改失败","/links/index");
			}
		}

	}