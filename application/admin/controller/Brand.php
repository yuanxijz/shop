<?php
/** 
    *后台品牌模块控制器类
    * 
    *
    * @author      3+1项目组.hzy
    * @version     2018-02-02 22:00
 */
namespace app\admin\controller;
//导入Controller
use think\Controller;
//导入Db
use think\Db;
class Brand extends Allow
{
    //调整类别顺序
    public function getCates()
    {
        //获取分类
        $data=Db::query("select *,concat(path,',',id) as paths from goods_cates order by paths");
        //遍历
        foreach($data as $key=>$value){
            //echo $value['path'].'<br>';
            //转换为数组
            $arr=explode(",",$value['path']);
            //获取逗号个数
            $len=count($arr)-1;
            //字符串重复函数
            $data[$key]['name']=str_repeat("---|",$len).$value['name'];
        }
       return $data;
    }
    //列表
    public function getIndex()
    {
        $request=request();
        $brand=Db::table('brands')->select();
       return $this->fetch('Brand/index',['brand'=>$brand]);
    }
    //加载添加模块
    public function getAdd()
    {
        //获取所有分类信息
        $cate=$this->getcates();
       return $this->fetch('Brand/add',['cate'=>$cate]);
    }
    //执行添加
    public function postInsert()
    {
        //创建请求对象
        $request=request();
        //调用验证方法
        $result=$this->validate($request->param(),'Brand');
        //输出错误信息
        if(true !== $result){
            $this->error($result,'/brand/add');
        }
        //先执行图片上传
        $file = $request->file('pic');
        $res = $this->validate(['file1'=>$file],['file1'=>'require|image|fileSize:1024000'],['file1.require'=>'上传文件为空','file1.image'=>'上传文件类型必须是图像类型','file1.fileSize'=>'图片大小不能超过1M']);//bug:图片上传大小限制没做
        if($res !== true){
            $this->error($res,"/brand/index");
        }
        $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
        if(!$info){
            $this->error($info,"/brand/index");
        }
        $savename = $info->getSaveName();
        //拼接图片路径
        // echo $savename;
        //20180201\874389dbab446daf794c2e4209d9d557.jpg
        //将$savename中\替换成/
        $savename = str_replace("\\", "/", $savename);
        //获取图片资源路径
        $picpath = '/public/uploads'.'/'.$savename;
        //echo $picpath;exit;
        //将获取的数据放入到一个数组
        $data=$request->only(['name','class_id','pic']);
        //将图片资源路径赋值给数组中pic字段
        $data['pic']=$picpath;
        //var_dump($data);exit;
        //插入数据库
        if(Db::table("brands")->insert($data)){
            //echo 1;
            $this->success('添加成功','/brand/index');
        }else{
            //echo "error";
            $this->error('添加失败','/brand/add');
        }
    }
    //执行删除
    public function getDelete($id)
    {
        //获取要删除图片数据
        $data = Db::table("brands")->where('id',$id)->find();
        //拼接要删除的图片资源路径
        $path = ROOT_PATH.'public'.DS.$data['pic'];
        //执行删除操作
        if(Db::table("brands")->where('id',$id)->delete()){
            //删除成功 同时删除图片
            unlink($path);
            $this->success("品牌删除成功","/brand/index");
        }else{
            $this->error("品牌删除失败","/brand/index");
        }
    }
    //执行修改
    public function getEdit()
    {
        $request=request();
        //获取id
        $id=$request->param('id');
        $data=$this->getcates();
        $info=Db::table('brands')->where('id',$id)->find();
        return $this->fetch('Brand/edit',['info'=>$info,'data'=>$data]);
    }
    //执行修改
    public function postUpdate($id){
        $request=request();
        //如果修改图片
        $file = $request->file('pics');
        if($file){
            //获取要删除的旧图片数据
            $data = Db::table("brands")->where('id',$id)->find();
            //拼接要删除的旧图片资源路径
            $path = ROOT_PATH.'public'.DS.$data['pic'];
            $res = $this->validate(['file1'=>$file],['file1'=>'image'],['file1.image'=>'上传文件类型必须是图像类型']);//bug:图片上传大小限制没做
            if($res !== true){
               $this->error($res,"/brand/index");
            }
            //上传成功 同时删除旧图片
            unlink($path);
            $info = $file->move(ROOT_PATH . 'public' . DS . 'uploads');
            if(!$info){
                $this->error($info,"/brand/index");
            }
            $savename = $info->getSaveName();
            //拼接图片路径
            // echo $savename;
            //将$savename中\替换成/
            $savename = str_replace("\\", "/", $savename);
            //获取图片资源路径
            $picpath = '/public/uploads'.'/'.$savename;    
        }else{
            //如果没有修改图片，获取图片资源路径
            $picpath = $_POST['pic'];
        }
        $data=$request->only(['name','class_id','pic']);
        //将图片资源路径赋值给数组中pic字段
        $data['pic']=$picpath;
        //var_dump($data);
        if(Db::table("brands")->where("id",$id)->update($data)){
            $this->success("修改成功","/brand/index");
        }else{
            $this->error("修改失败","/brand/index");
        }
    }
}
