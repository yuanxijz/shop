<?php
namespace app\admin\model;
//导入核心的model类
use think\Model;
class Admins extends Model{
	//设置模型对应的数据表
	protected $table="admin_user";
	//获取器 对字段数据(状态字段)做处理
	public function getStatusAttr($value){
		$status=[0=>'禁用',1=>'正常'];
		return $status[$value];
	}
} 
 ?>