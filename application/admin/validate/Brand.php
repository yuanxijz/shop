<?php
/** 
	*品牌添加验证
	* 
	*
	* @author      3+1项目组.hzy
	* @version     2018-02-02 15:23
*/

	namespace app\admin\validate;
	use think\Validate;
	class Brand extends Validate{

		//验证规则
		protected $rule=[
			'name'=>'require',
			'class_id'=>'require',
		];
		//默认提示信息
		protected $message=[
			'name.require'=>'品牌名称不能为空',
			'class_id.require'=>'请选择分类',
		];
		//验证场景
		protected $scene=[
			'add'=>['name','class_id'],
		];
	}
	

?>