<?php
/** 
	*商品添加验证
	* 
	*
	* @author      3+1项目组.hzy
	* @version     2018-02-03 19:00
*/

	namespace app\admin\validate;
	use think\Validate;
	class Goods extends Validate{

		//验证规则
		protected $rule=[
			'name'=>'require|unique:goods',
			'color'=>'require',
			'size'=>'require',
			'store'=>'require',
			'brands_id'=>'require',
		];
		//默认提示信息
		protected $message=[
			'name.require'=>'商品名称不能为空',
			'name.unique'=>'商品名称已经存在，请更换！',
			'color.require'=>'商品颜色不能为空',
			'size.require'=>'商品尺寸不能为空',
			'store.require'=>'商品库存不能为空',
			'brands_id.require'=>'请选择品牌',
		];
		//验证场景
		protected $scene=[
			'add'=>['name','color','size','store','brands_id'],
		];
	}
	

?>