<?php 
/** 
	*User添加验证
	* 
	*
	* @author      3+1项目组.yuanxi
	* @version     2018-02-28 23:41
*/

	namespace app\admin\validate;
	//导入Validate (核心验证类)
	use think\Validate;
	class Members extends Validate{
		//规则
		protected $rule=[
			'username'=>'require|regex:\w{4,8}|unique:admin_user',
			'password'=>'require',
			'repassword'=>'require|confirm:password',
			'email'=>'require|email|unique:admin_user',
		];

		//规则的描述
		protected $message=[
			'username.require'=>'用户名不能为空',
			'username.regex'=>'用户名必须是4-8位的任意的数字字母下划线',
			'username.unique'=>'用户名重复',
			'password.require'=>'密码不能为空',
			'repassword'=>'重复密码不能为空',
			'repassword.confirm'=>'两次密码不一致',
			'email.require'=>'邮箱不能为空',
			'email.email'=>'邮箱格式不对',
			'email.unique'=>'邮箱重复',
		];

		//验证场景
		protected $scene=[
			'add'=>['username','email','password'],
		];

	}
 ?>