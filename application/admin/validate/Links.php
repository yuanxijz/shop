<?php
/** 
	*友情链接添加验证
	* 
	*
	* @author      3+1项目组.yuanxi
	* @version     2018-02-02 15:04
*/

	namespace app\admin\validate;
	use think\Validate;
	class Links extends Validate{

		//验证规则
		protected $rule=[
			'name'=>'require|unique:links',
			'linkurl'=>'require|regex:/http:\/\//|unique:links',
			'linkemail'=>'require|email',
		];
		//默认提示信息
		protected $message=[
			'name.require'=>'友情链接名称为空',
			'name.unique'=>'友情链接名称已经存在，请更换！',
			'linkurl.require'=>'友情链接地址为空',
			'linkurl.regex'=>'友情链接地址必须以http://开头',
			'linkurl.unique'=>'友情链接地址已经存在，请不要重复添加',
			'linkemail.require'=>'邮箱为空',
			'linkemail.email'=>'邮箱格式不正确',
		];
		//验证场景
		protected $scene=[
			'add'=>['name','linkurl','linkeamil'],
		];
	}
	

?>