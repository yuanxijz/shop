<?php
/** 
	*Banner添加验证
	* 
	*
	* @author      3+1项目组.yuanxi
	* @version     2018-02-01 09:23
*/

	namespace app\admin\validate;
	use think\Validate;
	class Banners extends Validate{

		//验证规则
		protected $rule=[
			'name'=>'require|unique:banners',
			'url'=>'require|regex:/http:\/\//',
		];
		//默认提示信息
		protected $message=[
			'name.require'=>'Banner名称为空',
			'name.unique'=>'Banner名称已经存在，请更换！',
			'url.require'=>'Banner链接地址为空',
			'url.regex'=>'Banner链接地址必须以http://开头',
		];
		//验证场景
		protected $scene=[
			'add'=>['name','url'],
		];
	}
	

?>