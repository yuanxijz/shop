<?php 
/** 
	*User添加验证
	* 
	*
	* @author      3+1项目组.lh
	* @version     2018-02-02 10:23
*/

	namespace app\admin\validate;
	//导入Validate (核心验证类)
	use think\Validate;
	class Lists extends Validate{
		//规则
		protected $rule=[
			'name'=>'require',
			'mname'=>'require',
			'aname'=>'require',
			
		];

		//规则的描述
		protected $message=[
			'name.require'=>'用户名不能为空',
			'name.unique'=>'用户名重复',
			'mname.require'=>'控制器不能为空',
			'aname.require'=>'方法不能为空',
		];

		//验证场景
		protected $scene=[
			'add'=>['name','mname','aname'],
		];

	}
 ?>