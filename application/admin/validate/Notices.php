<?php
/** 
	*公告添加验证
	* 
	*
	* @author      3+1项目组.yuanxi
	* @version     2018-02-02 21:32
*/

	namespace app\admin\validate;
	use think\Validate;
	class Notices extends Validate{

		//验证规则
		protected $rule=[
			'title'=>'require|unique:notices',
			'content'=>'require',
		];
		//默认提示信息
		protected $message=[
			'title.require'=>'公告标题为空',
			'title.unique'=>'标题已经存在，请更换！',
			'content.require'=>'正文内容为空',
		];
		//验证场景
		protected $scene=[
			'add'=>['title','content'],
		];
	}
	

?>