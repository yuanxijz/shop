<?php
/** 
	*广告添加验证
	* 
	*
	* @author      3+1项目组.yuanxi
	* @version     2018-02-01 09:23
*/

	namespace app\admin\validate;
	use think\Validate;
	class Ads extends Validate{

		//验证规则
		protected $rule=[
			'name'=>'require|unique:ads',
			'adurl'=>'require|regex:/http:\/\//',
		];
		//默认提示信息
		protected $message=[
			'name.require'=>'广告名称为空',
			'name.unique'=>'广告名称已经存在，请更换！',
			'adurl.require'=>'广告链接地址为空',
			'adurl.regex'=>'广告链接地址必须以http://开头',
		];
		//验证场景
		protected $scene=[
			'add'=>['name','adurl'],
		];
	}
	

?>