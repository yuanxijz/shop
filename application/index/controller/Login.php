<?php
/** 
    *前台会员登录控制器类
    * 
    *注释要求 
    * @author      yuanxi
    * @version     2018-02-12 23:42
*/
namespace app\index\controller;
use think\Controller;
use think\Db;
use think\Session;
class Login extends Controller
{
    public function getLogin()
    {
    	return $this->fetch('Login/login');
    }
    public function postDologin(){
    	$request = request();
    	//调用验证器验证要插入的数据
    	$result = $this->validate($request->param(),'Login');
		if ($result !== true) {
			$this->error($result,"/login/login","",1);
		}
		//获取数据
        $username = $request->param("username");
		$password = $request->param("password");
		//加密密码
		$password = md5($password);
		//查询数据  先判断用户名
        $info = Db::table("members")->where("username", $username)->select();
        // var_dump($info);exit;
        if ($info !==null) {
            $res = Db::table("members")->where("username='{$username}' and password='{$password}'")->select();
            // echo "<pre>";
            // var_dump($res[0]['status']);exit;
            if ($res) {
                if ($res[0]['status']== 1) {
                    //把用户登录信息存储在session里面
                    Session::set('username',$res[0]['username']);
                    Session::set('uid',$res[0]['id']);
                    $this->success("登录成功","/myuser/myuser",'',1);
                }else{
                    $this->error("该用户尚未激活，请先完成激活","/login/login","",1);
                }                 
            }else{
                $this->error("密码不正确","/login/login","",1);
            }
        }else{
            $this->error("用户名不存在","/login/login","",1);
        }
       
		
    }
    //退出
    public function getLogout(){
        Session::delete('username');
        Session::delete('uid');
        $this->success("注销成功","/login/login",'',1);
    }
    //加载忘记密码操作
    public function getForget(){
        return $this->fetch("Login/forget");
    }
    //执行忘记密码
    public function postDoforget(){
        $request = request();
        $email = $request->param("email");
        // echo '<pre>';
        // var_dump($email);
        //发送邮件
        //获取数据
        $info = Db::table("members")->where("email",$email)->find();
        // echo "<pre>";
        // var_dump($info);exit;
        $res = sendmail($email,"密码找回","<a href='http://www.yuanxishop.com/login/reset?id={$info['id']}&token={$info['token']}'>点击我密码重置</a>");
        if($res){
            $this->success("重置密码邮件已经发送,请登录邮箱重置密码","https://mail.qq.com/");
        }
    }
    //密码重置
    public function getReset(){
        //获取传来的数据
        $id = $_GET['id'];
        $token = $_GET['token'];
        //根据ID取出数据库数据
        $info = Db::table("members")->where("id",$id)->find();
        // var_dump($info);exit;
        //对比token判断是否被劫持
        if ($info['token'] == $token) {
            //加载重置密码模板
            return $this->fetch("Login/reset",['info'=>$info]);
        }
    }
    //执行密码重置
    public function postDoreset(){
        $request = request();
        $res = $this->validate($request->only(['password','repassword']),"Reset");
        if ($res !== true) {
            $this->error($res,"/login/reset","",1);
        }
        //添加token防止被劫持
        $data['token'] = rand(1,10000);
        //获取密码
        $password = $request->param('password');
        //加密密码
        $password = md5($password);
        //把密码放入数组
        $data['password'] = $password;
        //获取ID
        $id = $request->param('id');
        // 修改数据，重置密码
        $info = Db::table("members")->where('id',$id)->update($data);
        if ($info) {
            $this->success("密码重置成功","/login/login","",1);
        }else{
            $this->error("密码重置失败","/login/login","",1);
        }
    }
}
