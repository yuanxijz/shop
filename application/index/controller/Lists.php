<?php
namespace app\index\controller;
use think\Controller;
use think\Db;
use think\paginator\driver\Bootstrap;
class Lists extends Controller
{
	//获取无限分类递归数据
	public function cates($pid)
    {
        $data=Db::table("goods_cates")->where('pid',$pid)->select();
        $data1=array();
        //遍历数据
        foreach($data as $key=>$value){
        	$value['id']=$this->cates($value['id']);
        	foreach ($value['id'] as $k => $val) {
        		$data1[]=$val;
        	}
        }
        $data1[]=$pid;
        return $data1;
    }
    public function getIndex()
    {
    	$request=request();
    	$id=$request->param('id');
    	//获取无限分类递归数据
        $cate=$this->cates($id);
        // echo "<pre>";
        // var_dump($cate);
        //查询品牌表
        foreach ($cate as $key => $value) {
        	$brands=Db::table("brands")->where('class_id',$value)->select();
        	// var_dump($brands);
        	foreach ($brands as $k => $val) {
        		$brand_arr[]=$val['id'];
        		// var_dump($val);
        	}
        }
        //查询商品表
        foreach ($brand_arr as $key => $value) {
        	$is=Db::table("goods")->where('brands_id',$value)->select();
        	foreach ($is as $k => $v) {
        		$goods[]=$v;
        	}
        }
        $query=array();
        if(input("post.CheckTime")){//条件查询
             $query=['CheckTime'=>input("post.CheckTime")];
        }
        elseif(input("get.CheckTime")){//分页查询
             $query=['CheckTime'=>input("get.CheckTime")];
        }
        else{//初始或者无条件的按钮操作
             $query=['CheckTime'=>input("get.CheckTime")];
        }
        $curpage = input('page') ? input('page') : 1;//当前第x页，有效值为：1,2,3,4,5...
        $listRow = 4;//每页4条记录
        $dataTo=array();
        $dataTo=array_chunk($goods,$listRow);
        $showdata=array();
        if($dataTo){
            $showdata = $dataTo[$curpage-1];
        }
        else{
            $showdata=null;
        }
        $p = Bootstrap::make($showdata, $listRow, $curpage, count($goods), false, [
            'var_page' => 'page',
            'path'     => url("/homelist/index/id/{$id}"),//这里根据需要修改url
            'query'    => $query,
            'fragment' => '',
        ]);
        $p->appends($_GET);
        $this->assign('plist', $p);
        $this->assign('plistpage', $p->render());
        $brands=Db::table("brands")->select();
        $goods=Db::table('goods')->order('addtime DESC')->limit(2)->select();
    	//加载模板 前台列表页
        return $this->fetch('Lists/index',['brands'=>$brands,'goods'=>$goods]);
    }
}
