<?php
namespace app\index\controller;
use think\Controller;
use think\Session;
use think\Db;
class Pay extends Controller
{
	public function postPay(){
			$sid = $_POST['sid'];
			// var_dump($sid);exit;
			//获取地址
			$adds = Db::table('place')->where('id',$sid)->find();
			$request = request();
			$user_id = Session::get('uid');
			//获取当前用户当前购物车数据
			$info = Db::table('carts')->where('user_id',$user_id)->select();
			$money = 0;
            $numss = 0;
            foreach($info as $key=>$value){
                $money += $value['price']*$value['num'];
                $numss += $value['num'];
            }
			$total = 0;
			$data = array();
        	//封装数据插入订单表
        	$data['time'] = time();
        	$data['orderid'] = time().rand(1000,9999);
        	$data['money'] = $money;
        	$data['status'] = 0;
        	$data['linkname'] = $adds['uname'];
        	$data['phone'] = $adds['phone'];
        	$data['uid'] = Session::get('uid');
        	$data['site'] = $adds['adds'].$adds['site'];
        	//插入订单表并返回订单id
        	$oid = Db::name('order')->insertGetId($data);
        	foreach($info as $key=>$value){
	            $xiaoji= $value['price']*$value['num'];
	            $total += $xiaoji;
	            $data2['pic'] = $value['pic'];
	            $data2['goodsname'] = $value['name'];
	            $data2['oid'] = $oid;
	            $data2['gnum'] = $value['num'];
	            $data2['price'] = $xiaoji;
	            $res = Db::table('orderdetail')->insert($data2);
	            if ($res !== null) {
	             //删除购物车当前用户当前商品数据
	                Db::table("carts")->where("user_id={$user_id} and shop_id={$value['shop_id']}")->delete();
	            }
        	}
        	$result['site'] = $data['site'];
        	$result['linkname'] = $data['linkname'];
        	$result['money'] = $total;
        	$result['orderid'] = $data['orderid'];
        	$result['phone'] = $adds['phone'];
        	//清除购物车数量
            Session::delete('carts_num');
			return $this->fetch('Pay/pay',['info'=>$info,'result'=>$result,'oid'=>$oid]);
		
	}
			
	//未在规定时间内付款取消订单
	public function getDelorder(){
		$request = request();
		$oid = $request->param('id');
		$ajax = Db::table('order')->where('id',$oid)->delete();
		if ($ajax) {
			$data = Db::table('orderdetail')->where('oid',$oid)->select();
			foreach ($data as $key => $value) {
				Db::table('orderdetail')->where('id',$value['id'])->delete();
			}
		}
		echo $ajax;
	}
	public function getPayok(){
		$request = request();
		$orderid = $request->param('orderid');
		$data = Db::table('order')->where("orderid",$orderid)->find();
		$infos = Db::table('orderdetail')->where("oid",$data['id'])->select();
		// var_dump($infos);exit;
		if (Db::table('order')->where("orderid",$orderid)->setField('status','1')) {
			foreach ($infos as $key => $value) {
				Db::table('orderdetail')->where("id",$value['id'])->setField('status','1');
			}
			$this->success("支付成功","/myuser/myuser","",1);
		}
	}
}

