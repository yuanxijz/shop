<?php
namespace app\index\controller;
use think\Controller;
use think\Db;
class Index extends Controller
{
    public function getIndex()
    {
    	//加载模板 前台首页
        return $this->fetch('Index/index');
    }
    //搜索
    public function postDosearch(){
    	//创建请求对象
    	$request=request();
    	//获取搜索的关键词
    	$s=$request->param('name');
        // var_dump($s);
    	$list=Db::table("goods")->where('name',"like","%".$s."%")->paginate(2);
    	return $this->fetch('Index/search',['list'=>$list,'request'=>$request->param(),'s'=>$s]);
    }
}
