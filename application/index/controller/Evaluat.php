<?php
namespace app\index\controller;
use think\Controller;
use think\Db;
use think\Session;
class Evaluat extends Controller
{
    //提交评价
    public function postInsert()
    {
    	$request = request();
        $odid = $request->param('id');
        // var_dump($infos);exit;
        //封装数据
        //满意度和评价心得
        $data = $request->only(['satisfact','content']);
        $data['odid'] = $odid;
        //获取当前用户的Session的UID
        $uid = Session::get('uid');
        $data['uid'] = $uid;
        $data['addtime'] = time();
        $data['status'] = 1;
        // var_dump($info);exit;
        if (Db::table('evaluats')->insert($data)) {
            $info['status'] = 4;
            if (Db::table('orderdetail')->where('id',$odid)->update($info)) {
                // 
                //如果评价成功之后，查询该商品所在的订单表的id=订单详情表的oid   的所有商品状态值
                //订单详情表的ID=评价表的odid
                //查询订单详情表该商品对应的数据
                $res = Db::table('orderdetail')->where('id',$odid)->find();
                //获取这条数据oid
                $oid = $res['oid'];
                //查询该oid对应的所有商品数据
                $res_infos = Db::table('orderdetail')->where('oid',$oid)->select();
                // var_dump($res_infos);exit;
                foreach ($res_infos as $key => $value) {
                    // var_dump($value['status']);
                    $tmp[] = $value['status'];
                   
                }
                  // var_dump($tmp);
                  $result = in_array(3,$tmp);
                  if (!$result) {
                    $order_infos['status'] = 4;
                    $ressss = Db::table('order')->where('id',$oid)->update($order_infos);
                  }
               
                $this->success("评价成功","/myorder/index/user_id/{$uid}",'',1);
              
            }
        }else{
            $this->error("评价失败","/myorder/index/user_id/{$uid}",'',1);
        }

    }
    //去评价
    public function getToevaluat(){
      $request = request();
      $oid = $request->param('id');
      $data = Db::table('orderdetail')->where('oid',$oid)->select();
      return $this->fetch('Evaluat/toevaluat',['data'=>$data]);
    }
    //评价具体商品
    public function getAddevaluat(){
      $request = request();
      //订单详情ID
      $id = $request->param('id');
      $data = Db::table('orderdetail')->where('id',$id)->find();
      $total = $data['price']*$data['gnum'];
      return $this->fetch('Evaluat/addevaluat',['data'=>$data,'total'=>$total]);
    }
    //查看评价
    public function getSeeevaluat(){
      $request = request();
      //订单详情ID
      $odid = $request->param('id');
      $data = Db::table('orderdetail')->where('id',$odid)->find();
      $evaluat = Db::table('evaluats')->where("odid='{$odid}' and status=1")->select();
      $evaluats = $evaluat[0];
      // var_dump($evaluats);exit;
      $total = $data['price']*$data['gnum'];
      return $this->fetch('Evaluat/seeevaluat',['data'=>$data,'total'=>$total,'evaluats'=>$evaluats]);
    }
    
}
