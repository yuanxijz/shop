<?php
namespace app\index\controller;
use think\Controller;
use think\Db;
class Goods extends Controller
{
    //获取无限分类递归数据
    public function cates($pid)
    {
        $data=Db::table("goods_cates")->where('pid',$pid)->select();
        $data1=array();
        //遍历数据
        foreach($data as $key=>$value){
            $value['shop']=$this->cates($value['id']);
            $data1[]=$value;
        }
        return $data1;
    }
    public function getIndex()
    {
        $request=request();
        $id=$request->param('id');
        $goods=Db::table("goods")->where('id',$id)->find();
        $goods1=Db::table('goods')->limit('6')->select();
        $goods2=Db::table('goods')->order('addtime DESC')->limit(2)->select();
        //获取无限分类递归数据
        $cates=$this->cates(0);
        // echo "<pre>";
        // var_dump($goods);
        // $data = Db::table('evaluats')->where()->select();
        //加载模板 详情页
        return $this->fetch('Goods/index',['goods'=>$goods,'goods1'=>$goods1,'cates'=>$cates,'goods2'=>$goods2]);
    }
}
