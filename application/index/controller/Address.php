<?php
/** 
    *前台会员个人中心地址管理控制器类
    * 
    *注释要求 
    * @author      yuanxi
    * @version     2018-02-28 19:17
*/
namespace app\index\controller;
use think\Controller;
use think\Db;
use think\Session;
class Address extends Allow
{
    public function getIndex()
    {
    	$uid = Session::get('uid');
    	$data = Db::table('place')->where('uid',$uid)->select();
        return $this->fetch('Address/index',['data'=>$data]);
    }
     //城市级联
    public function getAddress(){
        $request = request();
        $upid = $request->param('upid');
        $address = Db::table('district')->where('upid',$upid)->select();
        echo json_encode($address);
    }

    public function postInsertaddress(){
        //封装数据
        $uid = $_SESSION['think']['uid'];
        $_POST['uid'] = $uid;
        if(!empty($_POST['uname']) && !empty($_POST['phone']) && !empty($_POST['adds']) && !empty($_POST['site'])){
           Db::table('place')->insert($_POST); 
        }
        $site = Db::table('place')->where('uid',$_POST['uid'])->select();
        
        return $this->redirect('/address/index');
    }
    public function getDelete(){
    	$request = request();
    	$id = $request->param('id');
    	if (Db::table('place')->where('id',$id)->delete()) {
    		$this->success("删除成功","/address/index",'',1);
    	}else{
    		$this->error("删除失败","/address/index",'',1);
    	}
    }
    public function getEdit(){
    	$request = request();
    	$id = $request->param('id');
    	$data = Db::table('place')->where('id',$id)->find();
    	// var_dump($data);exit;
    	return $this->fetch("Address/edit",['data'=>$data]);
    }
    public function postDoedit(){
    	$request = request();
    	$id = $request->param('id');
    	$data = $request->only(['uname','phone','adds','site']);
    	// var_dump($infos);exit;
    	if (Db::table('place')->where('id',$id)->update($data)) {
    		$this->success("修改成功","/address/index",'',1);
    	}else{
    		$this->error("修改失败","/address/index",'',1);
    	}
    }
    //ajax设置默认地址
    public function getEditaddress(){
    	$request = request();
        $id = $request->param('uid');
        // echo $id;
        $data['status'] = 1;
        $info['status'] = 0;
        $uid = Session::get('uid');
        if (Db::table('place')->where('id',$id)->update($data)) {
         	$res = Db::table('place')->where("uid='{$uid}' and id!='{$id}'")->update($info);
         	if ($res) {
         		echo 1;
         	}
         } 
    }
}
