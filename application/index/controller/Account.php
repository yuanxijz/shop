<?php
namespace app\index\controller;
use think\Controller;
use think\Db;
use think\Session;
class Account extends Controller
{
    public function getAccount(){
        $request = request();
        $total = $request->param('total');
        $id = $request->param('id');
        //通过Session获取user_id 
        $user_id = Session::get('uid');
        //打印当前用户的购物车数据
        $data = Db::table("carts")->where("user_id={$user_id}")->select();
        $address=Db::table("place")->where("uid",$user_id)->select();
        $status_add=Db::table("place")->where("uid = '{$user_id}' and status = '1'")->find();
        // var_dump($status_add['status']);exit;
        if(empty($status_add)){
            $status_add['id'] = 0;
        }
        return $this->fetch('Account/account',['data'=>$data,'address'=>$address,'total'=>$total,"status_add"=>$status_add]);
    }

    //城市级联
    public function getAddress(){
        $request = request();
        $upid = $request->param('upid');
        $address = Db::table('district')->where('upid',$upid)->select();
        echo json_encode($address);
    }

    //添加收货地址
    public function postInsertaddress(){
       // echo "<pre>";
        // var_dump($_POST);
        //封装需要添加数据
        $data['uname']=$_POST['uname'];
        $data['phone']=$_POST['phone'];
        $data['adds']=$_POST['adds'];
        $data['site']=$_POST['site'];
        $data['uid']=Session::get('uid');
        $data['status']=0;
        //执行添加
        if(Db::table("place")->insert($data)){
            // echo "ok";
            $this->redirect("/account/account");
        }
    }

    //选中收货地址
    public function getSelected(){
        $request = request();
        $uid = Session::get('uid');
        $sid = $request->param('id');
        $info = Db::table('place')->where("uid=$uid and id=$sid")->setField('status','1');
        if($info){
            $infos = Db::table('place')->where("uid=$uid and id!=$sid")->setField('status','0');
        }
        echo $sid;
    }

    //设置默认地址
    public function getSite(){
        $request = request();
        $sid = $request->param('id');
        echo $sid;
    }

}
