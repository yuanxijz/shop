<?php
namespace app\index\controller;
use think\Controller;
use think\Db;
use think\Session;
class Cart extends Allow
{
    /**
     * 添加购物车
     * @author yuanxi
     * @Date   2018-03-04
     */
    public function getAdd()
    {
        $request = request();
        //获取商品ID和商品数量
        $shop_id = $request->param('id');
        // $num = $request->param('num');
        //通过Session获取以便购物车的user_id使用
        $user_id = Session::get('uid');
        //查询商品表
        $info = Db::table("goods")->where('id',$shop_id)->find();
        //获取当前用户购物车当前商品数据
        $cart_infos = Db::table("carts")->where("user_id='{$user_id}' and shop_id='{$shop_id}'")->select();
        if (!empty($cart_infos)) {
            $cart_infos[0]['num'] +=1;
            if($cart_infos[0]['num'] > $info['store']){
                $cart_infos[0]['num'] = $info['store'];
            }
            Db::table("carts")->where("id","{$cart_infos[0]['id']}")->update($cart_infos[0]);
        }else{
        // var_dump($info);
        //封装要添加的数据
         $data['user_id'] = $user_id;
         $data['shop_id'] = $shop_id;
         $data['name'] = $info['name'];
         $data['pic'] = $info['pic'];
         $data['num'] = 1;//默认设置为1
         $data['price'] = $info['price'];
        //添加数据到carts购物车表里面
          Db::table("carts")->insert($data);
        }
         $this->success("添加购物车成功","/cart/index","",1);
    }

    /**
     * 购物车列表页
     * @author yuanxi
     * @DateTime 2018-03-04T14:44:28+0800
     * @return   [type]                   [description]
     */
    public function getIndex(){
        $request = request();
        $user_id = Session::get('uid');
        $carts = Db::table('carts')->where('user_id',$user_id)->select();
        //总价和总数量
        $total = '';
        $sum = '';

        foreach ($carts as $key => $value) {
            $total += $value['price']*$value['num'];
            $sum += $value['num'];
        }
            // var_dump($tot_num);
            // exit;
        return $this->fetch("Cart/index",['carts'=>$carts,'total'=>$total,'sum'=>$sum]);
    }


    /**
     * 购物车减按钮--Ajax减
     * @author yuanxi
     * @DateTime 2018-03-04T15:06:01+0800
     * @return   array $arr
     */
    public function getSubtract(){
        $request = request();
        $id = $request->param('id');
        //通过Session获取user_id 
        $user_id = Session::get('uid');
        // echo $id;
        $info = Db::table("carts")->where("id",$id)->find();
        //数量减一
        $info['num'] -= 1;
        if($info['num']<1){
            $info['num'] = 1;
        }
        Db::table("carts")->where("id",$id)->update($info);
        //查询当前用户ID的当前所有购物车数据
        $data = Db::table("carts")->where("user_id",$user_id)->select();
        $sum = '';
        $total = '';
        foreach ($data as $key => $value) {
           // var_dump($value['num']);
           $sum += $value['num'];
           $total += ($value['num']*$value['price']);
        }
        Session::set('carts_num',$sum);
        $arr['tot']=$info['price']*$info['num'];
        $arr['num']=$info['num'];
        $arr['sum']=$sum;
        $arr['total']=$total;
        // var_dump($arr);
        // exit;
        echo json_encode($arr);
    }

    /**
     * 购物车加按钮--Ajax加
     * @author yuanxi
     * @DateTime 2018-03-04T14:46:10+0800
     * @return array  $arr
     */
    public function getPlus(){
        $request = request();
        $id = $request->param('id');
        //通过Session获取user_id 
        $user_id = Session::get('uid');
        // echo $id;
        $info = Db::table("carts")->where("id",$id)->find();
        //数量加一
        $info['num'] += 1;
        //获取商品库存  shops表的ID = 购物车表的shop_id
        $shop = Db::table("goods")->where('id',$info['shop_id'])->find();
        //对比：
        if ($info['num']>$shop['store']) {
            $info['num'] = $shop['store'];
        }
        Db::table("carts")->where("id",$id)->update($info);
        //查询当前用户ID的当前所有购物车数据
        $data = Db::table("carts")->where("user_id",$user_id)->select();
        $sum = '';
        $total = '';
        foreach ($data as $key => $value) {
           // var_dump($value['num']);
           $sum += $value['num'];
           $total += ($value['num']*$value['price']);
        }
        Session::set('carts_num',$sum);
        $arr['tot']=$info['price']*$info['num'];//小计
        $arr['num']=$info['num'];//数量
        $arr['sum']=$sum;//总数量
        $arr['total']=$total;//总价
        echo json_encode($arr);
    }

    /**
     * Ajax单条删除
     * @author yuanxi
     * @DateTime 2018-03-05T13:53:11+0800
     */
    public function getDelete(){
        $request = request();
        $id = $request->param('id');
        //通过Session获取user_id 
        $user_id = Session::get('uid');
        // echo $id;
        //删除指定ID的当前购物车数据
        if(Db::table("carts")->where('id',$id)->delete()){
          $del = 1;
        }
        //查询当前用户ID的当前所有购物车数据
        $data = Db::table("carts")->where("user_id",$user_id)->select();
        $sum = '';
        $total = '';
        foreach ($data as $key => $value) {
           // var_dump($value['num']);
           $sum += $value['num'];
           $total += ($value['num']*$value['price']);
        }
        if ($sum == null) {
            $sum = 0;
        }
        if ($total == null) {
            $total = 0;
        }
        Session::set('carts_num',$sum);
        $arr['del']=$del;
        $arr['sum']=$sum;
        $arr['total']=$total;     
        echo json_encode($arr);     
    }
    /**
     * 执行Ajax删除
     * @author yuanxi
     * @DateTime 2018-03-05T14:51:28+0800
     * @return   [type]                   [description]
     */
    public function getDel(){
        //通过Session获取user_id 
        $user_id = Session::get('uid');
        // echo "111";
        $arr=isset($_GET['arr'])?$_GET['arr']:'';
        if($arr==""){
            echo "请至少选中一条数据";
            exit;
        }
        // echo json_encode($arr);
        //遍历
        foreach($arr as $key=>$value){
            if(Db::table("carts")->where('id',"{$value}")->delete()){
                $del_all = 1;
            }
        }
        //查询当前用户ID的当前所有购物车数据
        $data = Db::table("carts")->where("user_id",$user_id)->select();
        $sum = '';
        $total = '';
        foreach ($data as $key => $value) {
           // var_dump($value['num']);
           $sum += $value['num'];
           $total += ($value['num']*$value['price']);
        }
        if ($sum == null) {
            $sum = 0;
        }
        if ($total == null) {
            $total = 0;
        }
        Session::set('carts_num',$sum);
        $arr['del_all']=$del_all;
        $arr['sum']=$sum;
        $arr['total']=$total;     
        echo json_encode($arr); 
    } 
}
