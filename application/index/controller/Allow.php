<?php
/** 
    *后台登录初始化文件
    * 
    *注释要求 
    * @author      yuanxi
    * @version     2018-02-13 10:00
*/
namespace app\index\controller;
use think\Controller;
use think\Session;
class Allow extends Controller{
	public function _initialize(){
		if (!Session::get('username')) {
			$this->error("请先登录","/login/login",'',1);
		}
	}
}