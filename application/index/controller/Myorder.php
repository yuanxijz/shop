<?php
/** 
    *前台会员个人中心我的订单控制器类
    * 
    *注释要求 
    * @author      yuanxi
    * @version     2018-02-13 00:00
*/
namespace app\index\controller;
use think\Controller;
use think\Db;
use think\Session;
class Myorder extends Allow
{
   //订单管理列表
   public function getIndex(){
      $request = request();
      $uid = $request->param("user_id");
      $s=$request->get('orderid');
      $data = Db::table("order")->where([
          'uid' => ['=',"{$uid}"],
          'orderid' => ['like',"%{$s}%"]
        ])->paginate(5);
      $order=Db::table('order')->where("uid='{$uid}'")->select();
      $num = count($order);
      for ($i=0;$i<$num;$i++){
            $info=Db::table('orderdetail')->where('oid',$order[$i]['id'])->select();
            $goods[]=$info;
        }
      //将订单下的商品插入
      foreach($order as $k=>$v){
            $order[$k]['goods_list'] = Db::table('orderdetail')->where('oid',$v['id'])->select();
      }     
      return $this->fetch("Myorder/index",['order'=>$order]);
   }
   
   
   //订单详情
   public function getDetail(){
      $request = request();
      $oid = $request->param('oid');
      $orderdetails = Db::table('orderdetail')->where('oid',$oid)->select();
      $orders = Db::table('order')->where('id',$oid)->find();
      return $this->fetch("Myorder/detail",['orderdetails'=>$orderdetails,'orders'=>$orders]);
   }
   //取消订单
   public function getCancel(){
      $request = request();
      $id = $request->param("id");
      $uid = Session::get('uid');
      $res = Db::table("order")->where('id',$id)->delete();
      if ($res) {
        $this->success("取消订单成功","/myorder/index/user_id/{$uid}","",1);
      }else{
        $this->error("取消订单失败","/myorder/index/user_id/{$uid}","",1);
      }
   }
   //修改购物车地址
   public function getEdit(){
      $request = request();
      $id = $request->param('id');
      $data = Db::table('order')->where('id',$id)->find();
      // var_dump($data);exit;
      return $this->fetch("Myorder/editaddress",['data'=>$data]);
    }
   //执行修改购物车地址
    public function postDoedit(){
      $request = request();
      $id = $request->param('id');
      // var_dump($id);exit;
      $adds = $request->param('adds');
      $site = $request->param('site');
      $data = $request->only(['linkname','phone']);
      $data['site'] = $adds.$site;
      $uid = Session::get('uid');
      if (Db::table('order')->where('id',$id)->update($data)) {
        $this->success("修改成功","/myorder/detail/oid/{$id}",'',1);
      }else{
        $this->error("修改失败","/myorder/detail/oid/{$id}",'',1);
      }
    }
    //订单详情和订单列表去付款方法
    public function getPay(){
        $request = request();
        //获取订单表ID值=订单详情表的oid
        $id = $request->param('id');
        //查询当前ID下的订单表数据
        $result = Db::table('order')->where('id',$id)->find();
        //查询当前oid下的订单详情表数据
        $info = Db::table('orderdetail')->where('oid',$id)->select();
        return $this->fetch("Myorder/pay",['result'=>$result,'info'=>$info]);
    }
    /**
     * 签收
     * @author yuanxi
     * @DateTime 2018-03-09T18:41:42+0800
     */
    public function getSign(){
      $request = request();
      $id = $request->param('id');
      $uid = Session::get('uid');
      $data = Db::table('orderdetail')->where('oid',$id)->select();
      if (Db::table('order')->where('id',$id)->setField('status',3)) {
          foreach ($data as $key => $value) {
            Db::table('orderdetail')->where('oid',$value['oid'])->setField('status',3);
          }
          $this->success("已签收","/myorder/index/user_id/{$uid}","",1);
      }
    }
}
