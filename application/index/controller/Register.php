<?php
/** 
    *前台会员注册控制器类
    * 
    *注释要求 
    * @author      yuanxi
    * @version     2018-02-12 21:42
*/
namespace app\index\controller;
use think\Controller;
use think\Db;
class Register extends Controller
{
    public function getRegister()
    {
    	return $this->fetch('Register/register');
    }
    public function postDoregister(){
    	$request = request();
    	//调用验证器验证要插入的数据
    	$result = $this->validate($request->param(),'Register');
		if ($result !== true) {
			$this->error($result,"/register/register");
		}
		//获取数据
		$data = $request->only(['username','password','email']);
		//获取时间以及加密密码、状态(默认注册为未激活0)
		$data['status'] = 0;
		$data['addtime'] = time();
		$data['password'] = md5($data['password']);
		$data['token'] = rand(1,10000);
		$id=Db::name("members")->insertGetId($data);
		//插入数据
		if ($id) {
			$res = sendmail($data['email'],"新用户激活","http://www.yuanxishop.com/register/jihuo?id={$id}&token={$data['token']}");
			if ($res) {
				$this->success("激活用户的邮件已发送","/login/login");
			}
		}
    }
    //激活用户
    public function getJihuo(){
    	//获取ID和token值
    	$id = $_GET['id'];
    	$token = $_GET['token'];
    	//获取当前数据的token值
    	$info = Db::table("members")->where("id",$id)->find();
    	//判断token是否一致防止被注入
    	if ($token == $info['token']) {
    		//封装插入:修改status为1激活状态
    		$arr['status'] = 1;
    		$arr['token'] = rand(1,10000);
    		if (Db::table("members")->where("id",$id)->update($arr)) {
    			$this->success("亲，您已经激活，请去登录！","/login/login");
    		}
    	}
    }
    //测试邮件发送
    // public function getSend(){
    // 	$s=sendmail("987985143@qq.com","o2o21激活","<a href=''>激活</a>");
    // 	if($s){
    // 		$this->success("邮件发送成功","https://mail.qq.com/");
    // 	}
    // }
}
