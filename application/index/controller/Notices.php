<?php
namespace app\index\controller;
use think\Controller;
use think\Db;
class Notices extends Controller
{
	//公告列表
    public function getIndex()
    {
    	$data = Db::table('notices')->where('status=1')->paginate(10);
    	$infos = Db::table('notices')->where('status=1')->select();
        return $this->fetch('Notices/index',['data'=>$data,'infos'=>$infos]);
    }
    //公告详情
    public function getArticle()
    {
    	$request = request();
    	$id = $request->param('id');
    	$notice = Db::table('notices')->where("status=1 and id='{$id}'")->find();
    	$data = Db::table('notices')->where('status=1')->select();
        return $this->fetch('Notices/article',['data'=>$data,'notice'=>$notice]);
    }
}
