<?php
/** 
    *前台会员个人中心控制器类
    * 
    *注释要求 
    * @author      yuanxi
    * @version     2018-02-13 00:00
*/
namespace app\index\controller;
use think\Controller;
use think\Db;
use think\Session;
class Myuser extends Allow
{
    public function getMyuser()
    {
    	$request = request();
    	$ip = $request->ip();
      $id = Session::get('uid');
      $data = Db::table('members')->where('id',$id)->find();
      // var_dump($data);
    	return $this->fetch('Myuser/myuser',['ip'=>$ip,'data'=>$data]);
    }
   //修改登录密码
   public function getEditPwd(){
   	   return $this->fetch("Myuser/myuser");
   }
   //执行修改
   public function postDoedit(){
   		//获取id
   		$id = Session::get("uid");
   		$request = request();
   			//接受新的密码
   			$new_password = $request->param("password");
   			$new_repassword = $request->param("repassword");
        // var_dump($new_password);exit;
        //接受电话号码和邮箱数据
        $tel = $request->param("tel");
        $email = $request->param("email");
        if (!empty($new_password)) {
            if ($new_password == $new_repassword) {
              //加密新密码
              $new_password = md5($new_password);
              $data['id'] = $id;
              $data['password'] = $new_password;
              //修改了密码时封装其它数据
              $data['tel'] = $tel;
              $data['email'] = $email;
              if (Db::table("members")->where("id={$id}")->update($data)) {
                $this->success("修改成功","/login/logout/","",1);
              }else{
                $this->error("修改失败","/myuser/myuser/id/{$id}","",1);
              }
            }else{
              $this->error("两次密码不一致请重新输入","/myuser/myuser/id/{$id}","",1);
            }     
        }else{
           //不修改密码只修改其它资料时封装其它数据
            $data['tel'] = $tel;
            $data['email'] = $email;
            if (Db::table("members")->where("id={$id}")->update($data)) {
              $this->success("修改成功","/myuser/myuser/id/{$id}","",1);
            }else{
              $this->error("修改失败","/myuser/myuser/id/{$id}","",1);
            }
        }
   			
   		

   }

  
}
