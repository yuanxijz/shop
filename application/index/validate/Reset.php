<?php
/** 
    *后台用户验证器类
    * 
    *
    * @author      yuanxi
    * @version     2018-02-03 20:56
 */

	namespace app\index\validate;
	use think\Validate;
	class Reset extends Validate{
		//验证规则
		protected $rule=[
			'password'=>'require',
			'repassword'=>'require|confirm:password',
		];
		//错误信息提示
		protected $message=[
			'password.require'=>'密码不能为空',
			'repassword'=>'重复密码不能为空',
			'repassword.confirm'=>'两次密码不一致',
		];
		//验证场景
		protected $scene=[
			'reset'=>['password'],
		];
	}