<?php
/** 
    *后台用户验证器类
    * 
    *
    * @author      yuanxi
    * @version     2018-02-03 20:56
 */

	namespace app\index\validate;
	use think\Validate;
	class Login extends Validate{
		//验证规则
		protected $rule=[
			'username'=>'require|regex:\w{4,8}',
			'password'=>'require',
		];
		//错误信息提示
		protected $message=[
			'username.require'=>'用户名不能为空',
			'username.regex'=>'用户名必须是4-8位的任意的数字字母下划线',
			'password.require'=>'密码不能为空',
		];
		//验证场景
		protected $scene=[
			'add'=>['username','password'],
		];
	}