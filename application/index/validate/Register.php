<?php
/** 
    *后台用户验证器类
    * 
    *
    * @author      yuanxi
    * @version     2018-02-03 20:56
 */

	namespace app\index\validate;
	use think\Validate;
	class Register extends Validate{
		//验证规则
		protected $rule=[
			'username'=>'require|regex:\w{4,8}|unique:members',
			'password'=>'require',
			'repassword'=>'require|confirm:password',
			'email'=>'require|email|unique:members',
		];
		//错误信息提示
		protected $message=[
			'username.require'=>'用户名不能为空',
			'username.regex'=>'用户名必须是4-8位的任意的数字字母下划线',
			'username.unique'=>'用户名重复',
			'password.require'=>'密码不能为空',
			'repassword'=>'重复密码不能为空',
			'repassword.confirm'=>'两次密码不一致',
			'email.require'=>'邮箱不能为空',
			'email.email'=>'邮箱格式不对',
			'email.unique'=>'邮箱重复',
		];
		//验证场景
		protected $scene=[
			'add'=>['username','email','password'],
		];
	}