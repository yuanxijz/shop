<?php
/** 
    *前台banner
    * 
    *注释要求 
    * @author      yuanxi
    * @version     2018-02-11 09:23
*/
namespace app\index\widget;
use think\Controller;
use think\Db;

class Banner extends Controller{
    public function banner(){
        $banners = Db::table("banners")->where("status","1")->order("id asc")->limit("3")->select();
        return $this->fetch("Banner/banner",['banners'=>$banners]);
    }
}
