<?php
namespace app\index\widget;
use think\Controller;
use think\Db;
use think\Session;
class Cate extends Controller
{
	//获取无限分类递归数据
	public function cates($pid)
    {
        $data=Db::table("goods_cates")->where('pid',$pid)->select();
        $data1=array();
        //遍历数据
        foreach($data as $key=>$value){
        	$value['shop']=$this->cates($value['id']);
        	$data1[]=$value;
        }
        return $data1;
    }
    public function header()
    {
        //获取无限分类递归数据
        $cate=$this->cates(0);
        // $goods=Db::table("goods")->select();
        // $brands=Db::table("brands")->select();
        //全局购物车数量  --- 如果session里面没有数据的话
        $request = request();
        $user_id = Session::get('uid');
        $carts = Db::table('carts')->where('user_id',$user_id)->select();
        //总数量
        $sum = '';
        foreach ($carts as $key => $value) {
            $sum += $value['num'];
        }
        return $this->fetch('Public/header',['cate'=>$cate,'sum'=>$sum]);
    }
    public function index()
    {
        //获取女装顶级分类的所有子类数据
        $cates1=$this->cate(1);
        //查询品牌表
        foreach ($cates1 as $key => $value) {
            $brands1=Db::table("brands")->where('class_id',$value)->select();
            // var_dump($brands);
            foreach ($brands1 as $k => $val) {
                $brand_arr1[]=$val['id'];
                // var_dump($val);
            }
        }
        //查询商品表
        foreach ($brand_arr1 as $key => $value) {
            $is1=Db::table("goods")->where('brands_id',$value)->select();
            foreach ($is1 as $k => $v) {
                $goods1[]=$v;
            }
        }
        //var_dump($goods1);exit;
        //获取男装顶级分类的所有子类数据
        $cates2=$this->cate(2);
        //查询品牌表
        foreach ($cates2 as $key => $value) {
            $brands2=Db::table("brands")->where('class_id',$value)->select();
            // var_dump($brands);
            foreach ($brands2 as $k => $val) {
                $brand_arr2[]=$val['id'];
                // var_dump($val);
            }
        }
        //查询商品表
        foreach ($brand_arr2 as $key => $value) {
            $is2=Db::table("goods")->where('brands_id',$value)->select();
            foreach ($is2 as $k => $v) {
                $goods2[]=$v;
            }
        }
        // var_dump($goods2);exit;
        //获取女鞋顶级分类的所有子类数据
        $cates3=$this->cate(14);
        //查询品牌表
        foreach ($cates3 as $key => $value) {
            $brands3=Db::table("brands")->where('class_id',$value)->select();
            // var_dump($brands);
            foreach ($brands3 as $k => $val) {
                $brand_arr3[]=$val['id'];
                // var_dump($val);
            }
        }
        //查询商品表
        foreach ($brand_arr3 as $key => $value) {
            $is3=Db::table("goods")->where('brands_id',$value)->select();
            foreach ($is3 as $k => $v) {
                $goods3[]=$v;
            }
        }
        // var_dump($goods3);exit;
        //获取无限分类递归数据
        $cate=$this->cates(0);
        $cate4=$this->cates(2);
        $cate5=$this->cates(14);
        $brands=Db::table("brands")->select();
        //加载首页
        return $this->fetch('Cate/cate',['cate'=>$cate,'goods1'=>$goods1,'brands'=>$brands,'goods2'=>$goods2,'goods3'=>$goods3,'cate4'=>$cate4,'cate5'=>$cate5]);
    }

    public function footer()
    {
        $data1 = Db::table('notices')->limit('0,4')->where('status=1')->select();
        $data2 = Db::table('notices')->limit('5,8')->where('status=1')->select();
        $data3 = Db::table('notices')->limit('8,3')->where('status=1')->select();
        $data4 = Db::table('notices')->limit('9,4')->where('status=1')->select();
        $data5 = Db::table('notices')->limit('14,3')->where('status=1')->select();
        //加载公共模板页面脚部
        return $this->fetch('Public/footer',['data1'=>$data1,'data2'=>$data2,'data3'=>$data3,'data4'=>$data4,'data5'=>$data5,]);
    }
    //会员中心左侧公共部分
    public function left(){
      return $this->fetch("Myuser/left");
    }
    //获取某顶级分类的所有子类数据
    public function cate($pid)
    {
        $data=Db::table("goods_cates")->where('pid',$pid)->select();
        $data1=array();
        //遍历数据
        foreach($data as $key=>$value){
            $value['id']=$this->cate($value['id']);
            foreach ($value['id'] as $k => $val) {
                $data1[]=$val;
            }
        }
        $data1[]=$pid;
        return $data1;
    }



}
