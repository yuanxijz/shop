<?php
namespace app\index\widget;
use think\Controller;
use think\Db;
class Ads extends Controller
{
	 public function ads(){
        $ads = Db::table("ads")->where("status","1")->order("id asc")->limit("1")->select();
        return $this->fetch("Ads/ads",['ads'=>$ads]);
    }
}
