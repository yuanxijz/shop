<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// return [
//     '__pattern__' => [
//         'name' => '\w+',
//     ],
//     '[hello]'     => [
//         ':id'   => ['index/hello', ['method' => 'get'], ['id' => '\d+']],
//         ':name' => ['index/hello', ['method' => 'post']],
//     ],

// ];

//导入路由类
use think\Route;
use think\Config;
//后台访问
Route::controller('/adminlogin','admin/Login');

//后台首页
Route::controller('/admin','admin/Admin');
Route::get("/admin.php","admin/Admin/getIndex");
//后台用户添加
Route::controller('/user','admin/User');
//管理员模块
Route::controller("/adminuser","admin/Admins");
//角色管理
Route::controller("/rolelist","admin/Rolelist");
//后台节点管理
Route::controller("/lists","admin/Lists");
//广告管理
Route::controller('/ads','admin/Ads');
//后台订单列表
Route::controller('/adminorder','admin/Order');
//后台订单详情
Route::controller('/orderdetail','admin/Orderdetail');
//无限分类模块
Route::controller('/cate','admin/Cate');
//后台品牌模块
Route::controller('/brand','admin/Brand');
//后台商品模块
Route::controller('/goods','admin/Goods');
//友情链接模块
Route::controller('/links','admin/Links');
//banner模块
Route::controller('/banners','admin/Banners');
//公告模块
Route::controller('/notices','admin/Notices');
//公告模块
Route::controller('/members','admin/Members');


//前台首页
Route::controller('/homeindex','index/Index');
Route::get("/","index/Index/getIndex");
Route::get("/index.php","index/Index/getIndex");
Route::get("/index.htm","index/Index/getIndex");
Route::controller('/index','index/Index');
//前台商品详情页
Route::controller('/homegoods','index/Goods');
//前台公告
Route::controller('/homenotices','index/Notices');
//购物车模块
Route::controller('/cart','index/Cart');
//前台下单
Route::controller('/account','index/Account');
Route::controller('/pay','index/Pay');
//前台会员注册
Route::controller('/register','index/Register');
//前台会员登录
Route::controller('/login','index/Login');
//前台会员中心
Route::controller('/myuser','index/Myuser');
//前台会员中心地址管理
Route::controller('/address','index/Address');
//前台会员订单管理
Route::controller('/myorder','index/Myorder');
//前台会员评价管理
Route::controller('/evaluat','index/Evaluat');
//前台列表页
Route::controller('/homelist','index/Lists');

